package kz.test.project.codegeneration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.test.project.codegeneration.model.payload.response.CodeResponse;
import kz.test.project.codegeneration.service.CodeService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class CodeControllerTest {

    @MockBean
    private CodeService codeService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void generate() throws Exception {
        CodeResponse codeResponse = new CodeResponse("a0a0" , "a0a1");
        Mockito.when(codeService.getCode()).thenReturn(codeResponse);
        mockMvc.perform(
                get("/api/code/generate")
        ).andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(codeResponse)));
    }
}