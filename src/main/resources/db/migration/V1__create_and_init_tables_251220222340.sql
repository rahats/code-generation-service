CREATE TABLE digits
(
    id          SERIAL PRIMARY KEY NOT NULL,
    value       VARCHAR(2)         NOT NULL,
    digit_order INT                NOT NULL
);

INSERT INTO digits(value, digit_order)
VALUES ('a0', (SELECT COALESCE(MAX(digit_order), 0) FROM digits) + 1);

INSERT INTO digits(value, digit_order)
VALUES ('a0', (SELECT MAX(digit_order) FROM digits) + 1);