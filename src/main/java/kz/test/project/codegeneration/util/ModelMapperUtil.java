package kz.test.project.codegeneration.util;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperUtil {
    private static ModelMapper modelMapper;

    public ModelMapperUtil(ModelMapper modelMapper) {
        ModelMapperUtil.modelMapper = modelMapper;
    }

    public static <T> T convertToDto(Object obj, Class<T> tClass) {
        return modelMapper.map(obj, tClass);
    }
}
