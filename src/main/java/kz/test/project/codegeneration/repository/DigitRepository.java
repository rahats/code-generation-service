package kz.test.project.codegeneration.repository;

import kz.test.project.codegeneration.model.entity.Digit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DigitRepository extends JpaRepository<Digit, Long> {
    List<Digit> findAllByOrderByDigitOrderDesc();
    boolean existsByDigitOrder(Integer digitOrder);
    Optional<Digit> findByDigitOrder(Integer digitOrder);
}
