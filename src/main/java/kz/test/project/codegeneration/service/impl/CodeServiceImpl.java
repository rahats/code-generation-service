package kz.test.project.codegeneration.service.impl;

import kz.test.project.codegeneration.model.dto.DigitCodeDto;
import kz.test.project.codegeneration.model.payload.response.CodeResponse;
import kz.test.project.codegeneration.service.CodeService;
import kz.test.project.codegeneration.service.DigitService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CodeServiceImpl implements CodeService {

    private static final List<String> ALPHABET = List.of("a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");

    private final DigitService digitService;

    @Override
    public CodeResponse getCode() {
        List<DigitCodeDto> digits = digitService.convertedDigits();
        String lastCode = digits.stream().sorted(Comparator.comparingInt(DigitCodeDto::getDigitOrder)).map(DigitCodeDto::getValue).collect(Collectors.joining(""));
        String generatedCode = generateCode(digits).stream().sorted(Comparator.comparingInt(DigitCodeDto::getDigitOrder)).map(
                DigitCodeDto::getValue
        ).collect(Collectors.joining(""));
        return new CodeResponse(lastCode, generatedCode);
    }

    private List<DigitCodeDto> generateCode(List<DigitCodeDto> list) {
        boolean overflow = false;
        for (DigitCodeDto dp : list) {
            overflow = checkOverflow(dp);
            if (!overflow) break;
        }
        int nextDigitOrder = list.stream().map(DigitCodeDto::getDigitOrder).max(Integer::compare).get() + 1;
        if (overflow) {
            DigitCodeDto newDigitCode = new DigitCodeDto("a0", nextDigitOrder);
            list.add(newDigitCode);
        }
        digitService.saveModifiedDigits(list);
        return list;
    }

    private boolean checkOverflow(DigitCodeDto dp) {
        char[] chars = dp.getValue().toCharArray();
        if (chars[0] == 'z' && Character.getNumericValue(chars[1]) == 9) {
            dp.setValue("a0");
            return true;
        } else if (Character.getNumericValue(chars[1]) == 9) {
            dp.setValue(ALPHABET.get(ALPHABET.indexOf(dp.getValue().substring(0, 1)) + 1) + 0);
        } else {
            dp.setValue(dp.getValue().substring(0, 1) + (Integer.parseInt(dp.getValue().substring(1)) + 1));
        }
        return false;
    }
}
