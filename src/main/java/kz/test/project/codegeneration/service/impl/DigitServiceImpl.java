package kz.test.project.codegeneration.service.impl;

import kz.test.project.codegeneration.exception.CustomNotFoundException;
import kz.test.project.codegeneration.model.dto.DigitCodeDto;
import kz.test.project.codegeneration.model.entity.Digit;
import kz.test.project.codegeneration.repository.DigitRepository;
import kz.test.project.codegeneration.service.DigitService;
import kz.test.project.codegeneration.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DigitServiceImpl implements DigitService {

    private final DigitRepository repository;

    @Override
    public List<DigitCodeDto> convertedDigits() {
        List<Digit> digits = repository.findAllByOrderByDigitOrderDesc();
        return digits.stream().map(digit -> ModelMapperUtil.convertToDto(digit, DigitCodeDto.class)).collect(Collectors.toList());
    }

    @Override
    public void saveModifiedDigits(List<DigitCodeDto> digitCodes) {
        digitCodes.forEach(digitCode -> {
            if (!repository.existsByDigitOrder(digitCode.getDigitOrder())) {
                repository.save(Digit.builder().value(digitCode.getValue()).digitOrder(digitCode.getDigitOrder()).build());
            } else {
                Digit digit = repository.findByDigitOrder(digitCode.getDigitOrder()).orElseThrow(() ->
                        new CustomNotFoundException(String.format("Digit with digitalOrder : %s not found",
                                digitCode.getDigitOrder())));
                digit.setValue(digitCode.getValue());
                repository.save(digit);
            }
        });
    }
}
