package kz.test.project.codegeneration.service;

import kz.test.project.codegeneration.model.payload.response.CodeResponse;

public interface CodeService {
    CodeResponse getCode();
}
