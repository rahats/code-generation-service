package kz.test.project.codegeneration.service;

import kz.test.project.codegeneration.model.dto.DigitCodeDto;

import java.util.List;

public interface DigitService {
    List<DigitCodeDto> convertedDigits();
    void saveModifiedDigits(List<DigitCodeDto> digitCodes);
}
