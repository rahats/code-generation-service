package kz.test.project.codegeneration.model.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "digits")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Digit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "value")
    private String value;

    @Column(name = "digit_order")
    private Integer digitOrder;
}
