package kz.test.project.codegeneration.model.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CodeResponse {
    private String lastCode;
    private String newGeneratedCode;
}
