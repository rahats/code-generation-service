package kz.test.project.codegeneration.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DigitCodeDto {
    private String value;
    private Integer digitOrder;
}
