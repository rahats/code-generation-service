package kz.test.project.codegeneration.exception.handler;

import kz.test.project.codegeneration.exception.CustomNotFoundException;
import kz.test.project.codegeneration.model.payload.response.FailureResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(CustomNotFoundException.class)
    public ResponseEntity<?> handleNotFoundException(CustomNotFoundException exception, HttpServletRequest request) {
        return new ResponseEntity<>(new FailureResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name(),
                exception.getMessage(), request.getRequestURI()), HttpStatus.NOT_FOUND);
    }
}
