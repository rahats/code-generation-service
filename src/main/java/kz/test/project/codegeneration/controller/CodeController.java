package kz.test.project.codegeneration.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kz.test.project.codegeneration.model.payload.response.CodeResponse;
import kz.test.project.codegeneration.service.CodeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/code")
@RequiredArgsConstructor
public class CodeController {

    private final CodeService service;

    @GetMapping(value = "/generate")
    @ApiResponses({
            @ApiResponse(code = 200, response = CodeResponse.class, message = "Success response")
    })
    @ApiImplicitParam(
            paramType = "header", name = "Accept-Language", value = "code",
            defaultValue = "en", allowableValues = "en")
    public ResponseEntity<?> generate() {
        return ResponseEntity.ok(service.getCode());
    }
}
